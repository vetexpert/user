FROM openjdk:11.0.10-jre-slim
COPY build/libs/user-*.jar /home/app/user.jar
ENTRYPOINT ["java", "-jar", "/home/app/user.jar"]
