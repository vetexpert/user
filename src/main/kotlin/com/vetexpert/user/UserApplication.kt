package com.vetexpert.user

import com.vetexpert.user.configuration.KeycloakProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(KeycloakProperties::class)
class UserApplication

fun main(args: Array<String>) {
    runApplication<UserApplication>(*args)
}
