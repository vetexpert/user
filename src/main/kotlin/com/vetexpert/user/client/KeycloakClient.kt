package com.vetexpert.user.client

import com.vetexpert.user.model.KeycloakUserListDto
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono


@Component
class KeycloakClient(
    private val keycloakWebClient: WebClient
) {
    private val searchUsersByUserGroupIdsPath = "/user-rest-extensions/users/search-by-user-group-ids"
    private val searchUsersByIdsPath = "/user-rest-extensions/users/search-by-ids"
    private val searchGroupsByUserIdPath = "/group-rest-extensions/groups/search-by-user-id"

    fun getUsersFromGroupByUserId(userId: String): Mono<KeycloakUserListDto> = keycloakWebClient.get()
        .uri("$searchUsersByUserGroupIdsPath?userId=$userId")
        .retrieve()
        .bodyToMono(KeycloakUserListDto::class.java)

    fun getUsersByIds(ids: String) =
        keycloakWebClient.get()
            .uri("$searchUsersByIdsPath?ids=$ids")
            .retrieve()
            .bodyToMono(KeycloakUserListDto::class.java)

    fun getGroupIdsByUserId(userId: String) =
        keycloakWebClient.get()
            .uri("$searchGroupsByUserIdPath?userId=$userId")
            .retrieve()
            .bodyToMono(List::class.java)
}
