package com.vetexpert.user.configuration

import org.keycloak.admin.client.Keycloak
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory

@Configuration
class KeycloakConfiguration(
    private val properties: KeycloakProperties
) {

    @Bean
    fun keycloak(): Keycloak = Keycloak.getInstance(
        properties.authServerUrl,
        properties.realm,
        properties.username,
        properties.password,
        properties.resource,
        properties.clientKeyPassword
    )

    @Bean
    fun keycloakWebClient(): WebClient = WebClient.builder()
        .uriBuilderFactory(
            DefaultUriBuilderFactory("${properties.authServerUrl}/realms/${properties.realm}")
        )
        .build()
}
