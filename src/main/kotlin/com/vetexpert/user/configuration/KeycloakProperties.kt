package com.vetexpert.user.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("keycloak")
data class KeycloakProperties(
    val username: String,
    val password: String,
    val authServerUrl: String,
    val realm: String,
    val resource: String,
    val clientKeyPassword: String
)
