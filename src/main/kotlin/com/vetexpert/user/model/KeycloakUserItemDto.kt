package com.vetexpert.user.model

data class KeycloakUserListDto(val users: List<KeycloakUserItemDto>)

data class KeycloakUserItemDto(
    val id: String,
    val lastName: String?,
    val firstName: String?,
    val middleName: String?,
    val roles: List<String>
)
