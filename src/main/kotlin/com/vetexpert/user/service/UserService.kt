package com.vetexpert.user.service

import com.vetexpert.keycloak.constants.Role
import com.vetexpert.proto.user.UserModel
import com.vetexpert.user.client.KeycloakClient
import com.vetexpert.user.model.KeycloakUserItemDto
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService(
    private val keycloakClient: KeycloakClient
) {

    fun getDoctors(userId: String): Mono<UserModel.UserList> =
        keycloakClient.getUsersFromGroupByUserId(userId).map { keycloakUserList ->
            UserModel.UserList.newBuilder().apply {
                addAllUsers(
                    keycloakUserList.users
                        .filter { it.roles.contains(Role.DOCTOR.name) }
                        .map {
                            userBuilder(it)
                        }
                )
            }.build()
        }.defaultIfEmpty(UserModel.UserList.getDefaultInstance())

    fun getUserByIds(ids: String): Mono<UserModel.UserList> {
        return keycloakClient.getUsersByIds(ids).map { keycloakUserList ->
            UserModel.UserList.newBuilder()
                .addAllUsers(keycloakUserList.users.map { userBuilder(it) })
                .build()
        }
    }

    fun getGroupIdsByUserId(userId: String): Mono<List<String>> =
        keycloakClient.getGroupIdsByUserId(userId) as Mono<List<String>>

    private fun userBuilder(it: KeycloakUserItemDto) = UserModel.User.newBuilder().apply {
        id = it.id
        fio = """
            ${it.lastName ?: ""} 
            ${it.firstName?.firstOrNull()?.plus(".") ?: ""} 
            ${it.middleName?.firstOrNull()?.plus(".") ?: ""}
        """.replace(" ", "").lines().joinToString(" ")
    }.build()
}
