package com.vetexpert.user.web

import com.vetexpert.user.web.handler.UserHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class Router(
    private val userHandler: UserHandler
) {

    @Bean
    fun route() = router {
        accept(MediaType.APPLICATION_OCTET_STREAM)
            .nest {
                "/user"
                    .nest {
                        GET("/doctors", userHandler::getDoctors)
                        GET("/list", userHandler::getUserByIds)
                    }
                "/group".nest {
                    GET("/byUserId/{userId}", userHandler::getGroupIdsByUserId)
                }
            }
    }

}
