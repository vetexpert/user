package com.vetexpert.user.web.handler

import com.vetexpert.user.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

@Component
class UserHandler(
    private val userService: UserService
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun getDoctors(request: ServerRequest): Mono<ServerResponse> =
        userService.getDoctors(request.queryParam("userId").orElseThrow())
            .flatMap {
                logger.info("${request.method()} ${request.path()}. Response: ${it.hide()}")
                ServerResponse.ok().bodyValue(it)
            }

    fun getUserByIds(request: ServerRequest): Mono<ServerResponse> =
        userService.getUserByIds(request.queryParam("ids").orElseThrow())
            .flatMap {
                logger.info("${request.method()} ${request.path()}. Response: ${it.hide()}")
                ServerResponse.ok().bodyValue(it)
            }

    fun getGroupIdsByUserId(request: ServerRequest): Mono<ServerResponse> =
        userService.getGroupIdsByUserId(request.pathVariable("userId"))
            .flatMap {
                logger.info("${request.method()} ${request.path()}. Response: ${it.hide()}")
                ServerResponse.ok().bodyValue(it)
            }

    fun Any.hide() = this.takeIf { logger.isTraceEnabled } ?: "***"
}
